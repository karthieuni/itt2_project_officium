Recreation checklist for group TTT
===================================================
Group name: Officium

Checklist
------------
- [x] Project plan completed
- [x] Minimal circuit schematic completed
- [ ] ATmega328 minimal system code with tempsensor board working
- [ ] ATMega328 to Rasperry Pi serial communication over UART working
- [ ] Raspberry pi configuration week06 requirements fulfilled
- [ ] Networked raspberry pi+atmega system behind a firewall accessible using SSH
- [ ] Temperature readout in celcius from the temperature sensor
- [ ] Raspberry API Server is able to serve temperature readings
- [ ] Virtual server installed
- [x] `group.md` complete according to specification
- [ ] Dashbaord communicating with API

Comments
-----------
* Assign roles for the issues, so the group members know whom is doing what.
* Organise the Gitlab repository, so it's easier to navigate to needed files (for ex. blueprint)
* Update the README.md in the root directory of your Gitlab repository.
* The guide is not very user-friendly, it's more like a description of the weekly tasks not a guide.
