## Step-by-step guide

Format a SD card with the *raspbian-stretch-lite* operating system - https://downloads.raspberrypi.org/raspbian_lite_latest

To get the reinstallation script<br > 
type ```https://gitlab.com/thaiswnielsen/itt2_project_officium/-/archive/master/itt2_project_officium-master.zip"```

Unzip the folder<br >
type ```unzip itt2_project_officium-master.zip```

Locate the script directory<br >
type ```cd /home/pi/Software/Reinstallation%20Script```

Make the script executable <br >
type ```sudo chmod 755 install.sh```

And run the script <br >
type ```sudo ./install.sh```
