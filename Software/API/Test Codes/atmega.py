#!usr/bin/env python3

import serial 

def read_led(ser):
	ser.write("led1val\n".encode())
	reply  = ser.readline().decode()
	print("-{}".format(reply))
	return reply

def readtemp(ser):
	ser.write("getadcval\n".encode())
	reply = ser.readline().decode()
	print("-{}".format(reply))
	return reply

def connect(port,baudrate):
	return serial.Serial(port, baudrate, timeout=1)
