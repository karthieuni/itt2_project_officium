<h2>Tree Diagram & Gitlab Page</h2>
Link to Gitlab page: https://thaiswnielsen.gitlab.io/itt2_project_officium/
<table align="center"><tr><td align="center" width="9999">
<img src="../Design Files/Tree Diagram/tree_diagram.png" align="center" width="600" alt="Project icon">

</td></tr></table>

<h2>Perspectives</h2>

Gitlab pages:

* Project: https://thaiswnielsen.gitlab.io/itt2_project_officium/
* Project (Network Configurations): https://gitlab.com/karthieuni/ITT2_week7_Officium_SII_FAN
* karthie.gitlab.io: https://karthieuni.gitlab.io/karthie.gitlab.io/
* henry.gitlab.io: https://henry1234.gitlab.io/henry.gitlab.io/
* thais.gitlab.io: https://thaiswnielsen.gitlab.io/thaiswnielsen.gitlab.io

<h2>Executive Overview</h2>
The aim of the project is to create a product for a customer. The group will be responsible for the implementation of the product, as well as the final documentation and the Project Plan.  
The document will propose a preliminary Project Plan for the student team involved, which they have to finalize and manage.  
This way the students can learn more about project management and Gitlab use in a simulated work environment.


<h2>What to do and when?</h2>
We will start the project with creating a project plan and reading up on information, as well as discussing assigned tasks like who, when and why.
Issues here on gitlab are assigned by the Project Manager.

<h2>Goals</h2>
<h3>High Level Diagram<h3>

![Diagram](../Design%20Files/Topology/diagram.png)

<h3>Setup<h3>

![Setup](../Software/Codes/arduino_setup.png)

The overall system that is going to be build looks as follows

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the seralconnection to the raspberry pi.
* Raspberry Pi: Minimal linux system relevant programs to upload/download data from the ATMega and to/from the APIs.
* Juniper router: Router to protect your internal system from the untrusted networks, and to enable access to the Internet and the "cloud servers"
* Reverse proxy/API gateway: This is a server/router that collects and protects the API endpoints and webservers implemented by each group.
* webservers: There are one webserver per group. It will include the REST API implementation and the user interface website.


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* A secure firewall enabling connections to the API.
* Docmentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation

The project itself will be divided into 3 phases and gitlab will be used as the project management platform.

<h2>Communications</h2>
Communication is essential in project and needs to be carefully focused on for the project to be successful.

* Scheduled stakeholder meetings on Gitlab every week
* Facebook group for regular messaging
* Project status reports
* Individual tasks for every stakeholder on Trello and Gitlab
* Gitlab issues has to be approved by everyone in the group before they're closed (commenting and linking the documents). 

<h2>Documentation</h2>
The Documentation folder includes the "Project-plan.md" which contains a more detailed plan on the project, and "User-manual.md" which is intended for the customer as a how to use guide.


* Link to the project plan: https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/project_plan.md

<h2>Programs</h2>
All files required to run the final product are in the "Programs" folder.

<h2>Team list</h2>

<table align="center">
    <tr>
        <th>Name</th>
        <th>Email</th>
    </tr>
    <tr>
        <td>Karthiebhan Mahendran</td>
        <td>kart0050@edu.eal.dk</td>
    </tr>
    <tr>
        <td>Thais W. Nielsen</td>
        <td>thai0051@edu.eal.dk</td>
    </tr>
    <tr>
        <td>Lauris Henrijs Valdovskis</td>
        <td>laur177n@edu.eal.dk</td>
    </tr>
</table>
