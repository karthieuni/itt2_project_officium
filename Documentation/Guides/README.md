**Purpose:** To recreate the exercises in the project

**Audience:** Developers

-----------------------------------------------------
**How to use the guide:**

**Follow the links in each week:**

1. Week 05 - Build veroboard with tempsensor, button and led
   * https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/Temperature_sensor__LED_and_push_button_circuit_guide.pdf
2. Week 06 - Raspberry, atmega and python
   * Raspberry, level shifter and atmega328p:
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/raspberry_atmega328p.pdf
   * Arduino UNO connection and temperature readout in celcius from the temperature sensor:
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/raspberry_arduino_uno.pdf 
3. Week 07 - Setting up juniper routers - (https://gitlab.com/karthieuni/ITT2_week7_Officium_SII_FAN)
   * The commands for the router configuration:
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/Router_Configuration.pdf
   * SRX Router (Backup/Restore):
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/SRX_backup_restore.pdf
   * Reinstallation script for the Raspberry Pi:
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/Reinstallation_script_&_mount_a_USB_(Raspberry_Pi).pdf
   * How to setup SSH on RPi guide:
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/SSH_and_wireless_access_point.pdf
   * How to setup gitbash and putting SSH keys in Gitlab:
   https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/Gitbash+Putting_SSH_key_on_Gitlab.pdf
   * To test if SSH (PuTTY, Gitbash), WinSCP (File Transfer Program) guide: 
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/Testing_SSH_connection__WinSCP__PuTTY.pdf
4. Week 08 - RESTful API service
   * https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/RESTful%20API%20service.pdf
5. Week 09 - Webserver
   * Setting up a virtual machine in a webserver & how to install nginx on the Raspberry Pi 
https://gitlab.com/thaiswnielsen/itt2_project_officium/blob/master/Documentation/Guides/PDF/VM_in_a_webserver___install_nginx_in_Raspberry_Pi.pdf
6. Week 10 (Recreate from documentation)
7. Week 11 (Status LED extension & Datalogger housing)
   * https://gitlab.com/thaiswnielsen/itt2_project_officium/tree/master/Design%20Files/Case%20Design
8. Week 12 (Creating a static page using Gitlab)
   * https://thaiswnielsen.gitlab.io/itt2_project_officium/
9. Week 13 (Product test)
