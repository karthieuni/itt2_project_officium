# End-Goal
Our end goal of this project is to create a Enterprise Temperature Monitoring System for ex. in a database center

## Measureables
<b> Humidity </b>
To measure how much water vapour is in the air. In a data center, maintaining ambient 
relative humidity levels between 45% and 55% is recommended for optimal 
performance and reliability.

<b> Temperature </b>
Maintaining an ambient temperature range of 68° to 75°F (20° to 24°C) is optimal for system reliability. 
This temperature range provides a safe buffer for equipment to operate in the event of air conditioning 
or HVAC equipment failure while making it easier to maintain a safe relative humidity level.

## Components

| Component      | Use                          |
|----------------|------------------------------|
| Thermsistor    | Measuring the Temperature    |
| Humidity sensor| Measuring the Humidity       |

## Reasoning

It is a safe and easy way to protect a data center from getting destroyed. 

## More information

## Email and SMS notifications
Alarm theft using temperature measurements.
Storage devices and other equipments in a data center won't get damaged. 