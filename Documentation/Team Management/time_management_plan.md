# Time Management Plan

This document defines and describes the element of the time that is planned for this project,
also how much time of work is expected by each member in the total memeber pool.

## Time Allocation timetable

| Week Number | Number of Members | Allocated Weekly Worktime | Weekly Team Worktime | Interruptible TimeMargin | Anticipate Work Performance | Total Time |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| 05 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | True | 48:00:00 |
| 06 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | True | 96:00:00 |
| 07 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | False | 144:00:00 |
| 08 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | False | 192:00:00 |
| 09 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | False | 192:00:00 |
| 10 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | False | 192:00:00 |
| 11 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | False | 192:00:00 |
| 12 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | False | 192:00:00 |
| 13 | 03 | 16:00:00 | 48:00:00 | 02:00:00 | False | 192:00:00 |

### Week Number
Week number is the number of the week that the row in the data table is currently is describing.

### Number of Members
This section is spimply just the numbers of members in the project that can preform any kind of work on the project, this is also how we can 
calulate the total worktime by taking the individual worktime and times it by the number of members. So, if one or more members are sick or 
unable to work in any capacity the total worktime is lowered by the amount of work that member could preform.

### Allocated Weekly Worktime

### Weekly Team Worktime
The weekly Team Worktime is based on how many members that are able to preform any kind of work times the worktime that is expected of them.
So, if a member is sick or in any other way is unable to preform any kind of work for the total workhours in a week that member is not counted as 
a asset to the team for the week. For each week where a member have not preformed as expected, a single detailed notation in a related document about this 
behaviour is written, after a few notations and lacking behaviour a meeting is warranted to fix any relating problems related to this behaviour.

### Interruptible TimeMargin

### Anticipate Work Performance
Anticipated work performance is a boolean question for determining if any memebers of the project team are lacking the amount of time or quallity of the work
that they are performing. If in any occasion one or more team memebers are lacking in any element of this nature a small note of the incident will be marked down
in a file on the Gitlab repository.

### Total Time