**Group Meetings - Minute points**

**Week 05 – Agenda**
1. Status on project (ie. show closed tasks in gitlab)
* The start of 2nd semester
2. Next steps (ie. show next tasks in gitlab)
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
---------------------------------------------------------------------------------
**Week 06 - Agenda**
1. Status on project (ie. show closed tasks in gitlab)
2. Next steps (ie. show next tasks in gitlab)
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
---------------------------------------------------------------------------------
**Week 07 - Agenda**
1. Status on project (ie. show closed tasks in gitlab)
2. Next steps (ie. show next tasks in gitlab)
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
---------------------------------------------------------------------------------
**Week 08 - Agenda**
1. Status on project (ie. show closed tasks in gitlab)
2. Next steps (ie. show next tasks in gitlab)
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
---------------------------------------------------------------------------------
**Week 09 - Agenda**
1. Status on project (ie. show closed tasks in gitlab)
2. Next steps (ie. show next tasks in gitlab)
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
------------------------------------------
* Install a virtual server in a cloud
* Consolidate and complete documentation
* Update group.md
------------------------------------------
**Week 10 - Agenda**
1. Status on project (ie. show closed tasks in gitlab)
2. Next steps (ie. show next tasks in gitlab)
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
---------------------------------------------------------------------------------
**Week 11 - Agenda**

1. Status on project (ie. show closed tasks in gitlab) 
* We are working to achieve our deliverables and reproduce the system created by group TTT.
2. Next steps (ie. show next tasks in gitlab)
* Group management
* Upload the checklist for group TTT
* Upload the checklist for our own group Officium 
* Fix the issues and assign roles
* Update the bug report
* Update the issues on Gitlab
* Work with status LED extension and datalogger housing

3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
* The roles have to be assigned in the Gitlab issues, so it will be more efficient way of working. 
4. Evaluation of test result from ww10 (show checklists)
* Send the checklist to the teachers, and notify this in the next meeting.
5. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
* Does the Raspberry Pi have to show status of LED blinking ? (Thais)
6. Any other business (AOB)
* No business
------------------------------------------
* Status LED extension - Missing
* Raspberry API client and Server API update to be able to handle Status LED extension - Missing
* Sketch showing housing for the minimum system hardware - Working on it 
* A rudimentary user manual for the system you will have in 3 weeks - Missing
---------------------------------------------------------------------------------
**Week 12 - Agenda**

1. Status on project (ie. show closed tasks in gitlab)
* We are catching up slowly with the deliverables, but are missing to add new issues.
2. Next steps (ie. show next tasks in gitlab)
* Learn Gitlab bash
* Create new issues & smart tasks
* More collaboration in the group
* Ask for teachers help
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
* We felt as if everyone had something to do, but the problem is we are not using Gitlab to show how we are doing it.
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
* Ask for teachers help in the future.
5. Any other business (AOB)

* A personal gitlab page
* Continue work on datalogger housing
---------------------------------------------------------------------------------
**Week 13 - Agenda**

1. Status on project (ie. show closed tasks in gitlab)
* Issues are not closed properly
* RESTful API needs to be tested on the virtualmachine
2. Next steps (ie. show next tasks in gitlab)
* Make the UART connection work again
* Make the dashboard work
* Test if dashboard can receive adc values from the temperature sensor 
3. Collaboration within the group (ie. any internal issues, fairness of workload,
communication)
* Dividing the tasks equally
4. Help needed or offered (ie. what help do you need and where do you feel you
can contribute to the class)
* Help with the UART connection with the ATmega328p and the Raspberry Pi.
5. Any other business (AOB)
* No

* Prepare product test for w14
* Finalize and print user manual
* Finalize datalogger housing